<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title>ORDER</title>
		<link rel="stylesheet" href="css/style.css"/>
	</head>
	<body>
		<div class="wrapper">
			<div class="order-list">
				<div class="order-list-inner">
					<div class="order-header">
						Заказ № 
						<xsl:value-of select = "order/order_id"/>
					</div>
					<xsl:for-each select="order/items/item">
						<div class="product">
							<div class="product-inner">
								<div class="name">
									<xsl:value-of select = "name"/>
								</div>
								<div class="price">
									Стоимость: 
									<xsl:value-of select = "price"/> ₽.
								</div>
							</div>
						</div>
					</xsl:for-each>
				</div>
			</div>
			<div class="client-info">
				<div class="client-info-inner">
					<div class="client-info-header">
						Информация о клиенте
					</div>
					<span>
						Фамилия: 
						<xsl:value-of select = "order/client/surname"/>
					</span>
					<span>
						Имя: 
						<xsl:value-of select = "order/client/name"/>
					</span>
					<span>
						Отчество: 
						<xsl:value-of select = "order/client/patronymic"/>
					</span>
					<span>
						Дата рождения: 
						<xsl:value-of select = "order/client/birthday"/>
					</span>
					<span>
						email: 
						<xsl:value-of select = "order/client/email"/>
					</span>
					<span>
						Номер телефона: 
						<xsl:value-of select = "order/client/phone"/>
					</span>
				</div>
			</div>
			<div class="address">
				<div class="address-inner">
					<div class="address-header">
						Адрес доставки
					</div>
					<span>
						Город: 
						<xsl:value-of select = "order/address/town"/>
					</span>
					<span>
						Улица: 
						<xsl:value-of select = "order/address/street"/>
					</span>
					<span>
						Дом: 
						<xsl:value-of select = "order/address/house"/>
					</span>
					<span>
						Подъезд: 
						<xsl:value-of select = "order/address/porch"/>
					</span>
					<span>
						Квартира: 
						<xsl:value-of select = "order/address/apartment"/>
					</span>
				</div>
			</div>
		</div>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>