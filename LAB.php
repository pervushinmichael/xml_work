<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>ORDER</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<?php 
		$order = simplexml_load_file("LAB.xml");
	?>
	<div class="wrapper">
		<div class="order-list">
			<div class="order-list-inner">
				<div class="order-header">
						Заказ № 
						<?php echo $order->order_id ?>
				</div>
				<?php 
					$sum = 0;
					foreach ($order->items->item as $product): 
				?>
				<div class="product">
					<div class="product-inner">
						<div class="name">
							<?php echo $product->name;?>
						</div>
						<div class="price">
							<?php 
								$sum+=$product->price;
								echo "Стоимость: ".$product->price." ₽.";
							?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<div class="product">
					<div class="product-inner">
						<div class="price">
							Общая стоимость: 
							<?php echo $sum ." ₽."; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="client-info">
			<div class="client-info-inner">
				<div class="client-info-header">
					Информация о клиенте
				</div>
				<span>
					Фамилия: 
					<?php echo $order->client->surname;?>
				</span>
				<span>
					Имя: 
					<?php echo $order->client->name;?>
				</span>
				<span>
					Отчество: 
					<?php echo $order->client->patronymic;?>
				</span>
				<span>
					Дата рождения: 
					<?php echo $order->client->birthday;?>
				</span>
				<span>
					email: 
					<?php echo $order->client->email;?>
				</span>
				<span>
					Номер телефона: 
					<?php echo $order->client->phone;?>
				</span>
			</div>
		</div>
		<div class="address">
			<div class="address-inner">
				<div class="address-header">
					Адрес доставки
				</div>
				<span>
					Город: 
					<?php echo $order->address->town;?>
				</span>
				<span>
					Улица: 
					<?php echo $order->address->street;?>
				</span>
				<span>
					Дом: 
					<?php echo $order->address->house;?>
				</span>
				<span>
					Подъезд: 
					<?php echo $order->address->porch;?>
				</span>
				<span>
					Квартира: 
					<?php echo $order->address->apartment;?>
				</span>
			</div>
		</div>
	</div>
</body>
</html>